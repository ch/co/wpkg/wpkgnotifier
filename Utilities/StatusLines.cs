﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WpkgUtilities
{
    public class StatusLines : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<string> lines = new List<string>();

        public string Lines
        {
            get
            {
                return string.Join(Environment.NewLine, lines);
            }
            set
            {
                lines.Add(value);
                NotifyPropertyChanged("lines");
            }
        }

        public void Add(string line)
        {
            lines.Add(line);
            NotifyPropertyChanged("lines");
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }
    }
}
