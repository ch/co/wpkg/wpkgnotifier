﻿using System.ComponentModel;

namespace WpkgUtilities
{
    // Provide a simple wrapper around a string which allows us to bind 
    // a property to a textbox in a form element
    public class WpkgStatusMessage : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string StatusMessage { set; get; } = "The status of software installed on this computer has not been yet been checked since the last boot.";

        public void SetMessage(string message)
        {
            StatusMessage = message;
            NotifyPropertyChanged("StatusMessage");
        }
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
