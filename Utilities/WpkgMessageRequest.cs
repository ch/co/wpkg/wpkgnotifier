﻿namespace WpkgUtilities
{
    public enum WpkgMessageRequestType
    {
        DO_SYNC,
        RUN_QUERY,
        DONE,
    }
    public class WpkgMessageRequest
    {
        public WpkgMessageRequestType Type { get; set; }

        public WpkgMessageRequest(WpkgMessageRequestType type)
        {
            this.Type = type;
        }
    }
}
