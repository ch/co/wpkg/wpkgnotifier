﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WpkgUtilities
{
    public class WpkgQueryResult : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public List<string> PendingInstallations { get; set; }
        public List<string> PendingUpgrades { get; set; }
        public List<string> PendingRemovals { get; set; }
        public DateTime LastChecked { get; set; }
        private bool CheckPerformed = false;

        public bool IsEmpty()
        {
            return (!PendingInstallations.Any()) && (!PendingRemovals.Any()) && (!PendingUpgrades.Any());
        }

        // We want to bind ToString() to a form textbox, but we
        // can only bind class properties - thus we need a simple
        // wrapper
        public string StatusText
        {
            get
            {
                return ToString();
            }
        }

        public void SetResults(List<string> installations, List<string> upgrades, List<string> removals)
        {
            PendingInstallations = new List<string>(installations);
            PendingUpgrades = new List<string>(upgrades);
            PendingRemovals = new List<string>(removals);

            LastChecked = DateTime.Now;

            CheckPerformed = true;
            NotifyPropertyChanged("StatusText");
        }

        public override string ToString()
        {
            string msg = "";

            if (CheckPerformed)
            {
                if (PendingUpgrades.Any())
                {
                    msg += String.Format("Upgrades pending for: {0}. ", String.Join(", ", PendingUpgrades));
                }

                if (PendingInstallations.Any())
                {
                    msg += String.Format("Installation pending for: {0}. ", String.Join(", ", PendingInstallations));
                }
                if (PendingRemovals.Any())
                {
                    msg += String.Format("Removal pending for: {0}. ", String.Join(", ", PendingRemovals));
                }

                if (msg == "")
                {
                    msg = "No changes needed. ";
                }

                msg += String.Format(" (Last checked: {0})", LastChecked.ToString("dd/MM/yy HH:mm:ss"));
            }
            else
            {
                msg = "The status of installed software has not yet been checked.";
            }
            return msg;
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
