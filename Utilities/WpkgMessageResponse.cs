﻿using System.Runtime.Serialization;

namespace WpkgUtilities
{
    [DataContract]
    public class WpkgMessageResponse
    {
        [DataMember]
        public string Message { get; set; }

        public WpkgMessageResponse()
        {

        }
        public WpkgMessageResponse(string msg)
        {
            Message = msg;
        }

    }
}
