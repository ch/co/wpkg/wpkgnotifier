﻿namespace WpkgUtilities
{
    public enum WpkgServiceState
    {
        IDLE,
        BUSY_QUERYING,
        BUSY_SYNCING,
        UPDATES_PENDING,
        DISCONNECTED
    }
}
