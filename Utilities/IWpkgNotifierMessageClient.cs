﻿using System.ServiceModel;

namespace WpkgUtilities
{
    public interface IWpkgNotifierMessageClient
    {
        [OperationContract(IsOneWay = true)]
        void LogMessage(string msg);

        [OperationContract(IsOneWay = true)]
        void LogQueryResult(WpkgQueryResult result);

        [OperationContract(IsOneWay = true)]
        void SetStatus(WpkgServiceState status);

        [OperationContract(IsOneWay = true)]
        void Shutdown(string msg = null);
    }
}