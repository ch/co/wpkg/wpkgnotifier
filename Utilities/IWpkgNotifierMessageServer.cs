﻿using System;
using System.ServiceModel;

namespace WpkgUtilities
{

    [ServiceContract(CallbackContract = typeof(IWpkgNotifierMessageClient))]
    public interface IWpkgNotifierMessageServer
    {
        [OperationContract(IsOneWay = true)]
        void Register(int pid, Version version);

        [OperationContract(IsOneWay = true)]
        void Unregister(int pid);

        [OperationContract(IsOneWay = true)]
        void ProcessRequest(WpkgMessageRequestType type);
    }
}
