﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using WpkgUtilities;

namespace WpkgInstaller
{
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public int dwServiceType;
        public ServiceState dwCurrentState;
        public int dwControlsAccepted;
        public int dwWin32ExitCode;
        public int dwServiceSpecificExitCode;
        public int dwCheckPoint;
        public int dwWaitHint;
    };

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public partial class WpkgInstaller : ServiceBase, IWpkgNotifierMessageServer
    {

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);

        private Version version;

        private const string InstallationPendingText = "Action:            Installation pending";
        private const string RemovalPendingText = "Action:            Remove pending";
        private const string UpgradePendingText = "Action:            Upgrade pending";

        private Dictionary<int, IWpkgNotifierMessageClient> clients;

        private string WpkgCmd;
        private int QueryIntervalSecs;
        private static System.Timers.Timer queryTimer;

        private string queriedPackageName;
        private List<string> pendingInstallations = new List<string>();
        private List<string> pendingRemovals = new List<string>();
        private List<string> pendingUpgrades = new List<string>();

        private static readonly object stateLock = new object();
        private WpkgServiceState state;

        public WpkgInstaller()
        {
            InitializeComponent();

            version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

            eventLog = new EventLog();
            clients = new Dictionary<int, IWpkgNotifierMessageClient>();
            string eventSourceName = "WpkgInstaller Service";
            string logName = "WpkgNotifier";

            if (!EventLog.SourceExists(eventSourceName))
            {
                EventLog.CreateEventSource(eventSourceName, logName);
            }

            eventLog.Source = eventSourceName;
            eventLog.Log = logName;

            LoadSettings();

            OpenMessageChannel();

            StartQueryTimer();

        }

        private void StartQueryTimer()
        {
            queryTimer = new System.Timers.Timer(QueryIntervalSecs);
            queryTimer.Elapsed += (Object source, ElapsedEventArgs e) =>
            {
                eventLog.WriteEntry("Running scheduled query");
                ProcessRequest(WpkgMessageRequestType.RUN_QUERY);
            };
            queryTimer.AutoReset = true;
            queryTimer.Enabled = true;
            eventLog.WriteEntry("Timer enabled with interval " + QueryIntervalSecs);
        }
        public WpkgServiceState State
        {
            get
            {
                lock (stateLock)
                {
                    return state;
                }
            }
            set
            {
                lock (stateLock)
                {
                    SetClientsState(value);
                    state = value;
                }
            }
        }

        protected void LoadSettings()
        {
            WpkgCmd = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\Software\\WPKG\\WpkgNotifier", "WpkgCommand", null);
            QueryIntervalSecs = 1000 * System.Convert.ToInt32(Registry.GetValue("HKEY_LOCAL_MACHINE\\Software\\WPKG\\WpkgNotifier", "QueryIntervalSecs", 3600));
        }
        protected override void OnStart(string[] args)
        {

            ServiceStatus serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_START_PENDING,
                dwWaitHint = 100000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(ServiceHandle, ref serviceStatus);

            eventLog.WriteEntry(String.Format("Service v{0} started", version.ToString()), EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            ServiceStatus serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_STOP_PENDING,
                dwWaitHint = 100000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);

            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(ServiceHandle, ref serviceStatus);

            eventLog.WriteEntry("Service stopped", EventLogEntryType.Information);
        }

        private void RunWpkgSync()
        {

            if (!CheckIfSafeToRun(WpkgMessageRequestType.DO_SYNC))
            {
                return;
            }

            eventLog.WriteEntry("Starting sync", EventLogEntryType.Information);

            ThreadStart cmdThread = () =>
            {
                State = WpkgServiceState.BUSY_SYNCING;
            };

            cmdThread += GetWpkgCommandThread("/synchronize");

            // Transition back to IDLE after the /synchronize thread finishes. 
            cmdThread += () =>
            {
                State = WpkgServiceState.IDLE;
            };

            cmdThread += () =>
            {
                // re-query after the sync has finished - hopefully this will report
                // back to the client that there are no longer any pending changes.
                ProcessRequest(WpkgMessageRequestType.RUN_QUERY);
            };

            Thread thread = new Thread(cmdThread);
            thread.Start();
        }

        private void RunWpkgQuery()
        {
            if (!CheckIfSafeToRun(WpkgMessageRequestType.RUN_QUERY))
            {
                return;
            }

            eventLog.WriteEntry("Starting query", EventLogEntryType.Information);
            SendLogMessageToClients("Query started");

            WpkgQueryResult result = new WpkgQueryResult();
            pendingInstallations.Clear();
            pendingRemovals.Clear();
            pendingUpgrades.Clear();

            ThreadStart cmdThread = () =>
            {
                State = WpkgServiceState.BUSY_QUERYING;
            };

            cmdThread += GetWpkgCommandThread("/query:m /logfilePattern:wpkg-notifier.log", ProcessQueryCmdStdout);

            cmdThread += () =>
            {
                result.SetResults(pendingInstallations, pendingUpgrades, pendingRemovals);
                SendQueryResultToClients(result);
                SendLogMessageToClients("Query finished");
                if (result.IsEmpty())
                {
                    State = WpkgServiceState.IDLE;
                }
                else
                {
                    State = WpkgServiceState.UPDATES_PENDING;
                }
            };

            Thread thread = new Thread(cmdThread);
            thread.Start();
        }


        private void CheckIfConnected()
        {
            if (State == WpkgServiceState.BUSY_SYNCING || State == WpkgServiceState.BUSY_QUERYING)
            {
                // N.B. this code branch ought to never be reached. But if we are already
                // BUSY it means a command thread is executing, and so we'll just return
                // here and let that thread finish.
                return;
            }

            if (!File.Exists(WpkgCmd))
            {
                State = WpkgServiceState.DISCONNECTED;
            }
            else if (State == WpkgServiceState.DISCONNECTED)
            {
                State = WpkgServiceState.IDLE;
            }
        }

        private bool CheckIfSafeToRun(WpkgMessageRequestType type)
        {
            bool safe;

            CheckIfConnected();

            switch (State)
            {
                case WpkgServiceState.DISCONNECTED:
                    eventLog.WriteEntry(String.Format("Ignoring {0} request because state is disconnected", type));
                    safe = false;
                    break;

                case WpkgServiceState.BUSY_QUERYING:
                    eventLog.WriteEntry(String.Format("Ignoring {0} request because the service is already busy querying", type));
                    safe = false;
                    break;

                case WpkgServiceState.BUSY_SYNCING:
                    eventLog.WriteEntry(String.Format("Ignoring {0} request because the service is already busy syncing", type));
                    safe = false;
                    break;

                default:
                    eventLog.WriteEntry(String.Format("Processing {0} request.", type));
                    safe = true;
                    break;
            }

            return safe;
        }

        private ThreadStart GetWpkgCommandThread(string arg)
        {
            return GetWpkgCommandThread(arg, ProcessCommandStdout, ProcessCommandStderr);
        }

        private ThreadStart GetWpkgCommandThread(string arg, DataReceivedEventHandler stdoutDelegate)
        {
            return GetWpkgCommandThread(arg, stdoutDelegate, ProcessCommandStderr);
        }

        private ThreadStart GetWpkgCommandThread(string arg, DataReceivedEventHandler stdoutDelegate, DataReceivedEventHandler stderrDelegate)
        {

            ThreadStart thWorker = new ThreadStart(delegate
            {

                Process process = new Process();
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    WindowStyle = ProcessWindowStyle.Hidden,

                    FileName = Environment.GetEnvironmentVariable("comspec"),
                    Arguments = String.Format("/C {0} {1}", WpkgCmd, arg),
                    WorkingDirectory = Environment.GetEnvironmentVariable("temp"),

                    UseShellExecute = false,
                };

                process.StartInfo = startInfo;

                process.EnableRaisingEvents = true;
                process.OutputDataReceived += stdoutDelegate;
                process.ErrorDataReceived += stderrDelegate;

                process.Start();
                process.BeginErrorReadLine();
                process.BeginOutputReadLine();
                process.WaitForExit();

                String msg = String.Format("wpkg command finished, with exit code {0}", process.ExitCode);
                eventLog.WriteEntry(msg, EventLogEntryType.Information);
            });

            return thWorker;
        }

        private void ProcessCommandStdout(object sendingProcess, DataReceivedEventArgs e)
        {
            if (e != null && e.Data != null)
            {
                SendLogMessageToClients(e.Data);
                // sent e.Data back to clients
            }

        }

        private void ProcessCommandStderr(object sendingProcess, DataReceivedEventArgs e)
        {
            if (e != null && e.Data != null)
            {
                SendLogMessageToClients(e.Data);
                eventLog.WriteEntry(e.Data, EventLogEntryType.Error);
            }

        }

        private void ProcessQueryCmdStdout(object sendingProcess, DataReceivedEventArgs e)
        {
            if (e != null && e.Data != null)
            {
                SendLogMessageToClients(e.Data);
            }

            /**
             * Need to parse...
               Current profile packages:
               PuTTY
                   ID:                putty
                   Revision:          0.73.1
                   Action:            Installation pending
                   Reboot:            false
                   Execute:           -
                   Priority:          50
                   Status:            Installed
             * ... a line at a time. So we
             * 1) ignore the initial header, 
             * 2) otherwise, look for a string without leading whitespace which will be the package name,
             * 3) then, look for an exact match on the Action: line
             * TODO: the package name could theoretically start with whitespace, so a more intelligent
             * parser is needed.
             */
            if (!String.IsNullOrEmpty(e.Data)
                && !Char.IsWhiteSpace(e.Data, 0)
                && e.Data != "Current profile packages:")
            {
                queriedPackageName = e.Data;
            }

            if (!String.IsNullOrEmpty(queriedPackageName))
            {
                if (e.Data.Contains(InstallationPendingText))
                {
                    eventLog.WriteEntry(String.Format("Installation pending for {0}", queriedPackageName));
                    pendingInstallations.Add(queriedPackageName);
                    queriedPackageName = String.Empty;
                }
                else if (e.Data.Contains(RemovalPendingText))
                {
                    eventLog.WriteEntry(String.Format("Removal pending for {0}", queriedPackageName));
                    pendingRemovals.Add(queriedPackageName);
                    queriedPackageName = String.Empty;
                }
                else if (e.Data.Contains(UpgradePendingText))
                {
                    eventLog.WriteEntry(String.Format("Upgrade pending for {0}", queriedPackageName));
                    pendingUpgrades.Add(queriedPackageName);
                    queriedPackageName = String.Empty;
                }

            }
        }

        private void OpenMessageChannel()
        {
            try
            {
                Uri uri = new Uri("net.pipe://localhost/www.ch.cam.ac.uk/WpkgNotifier");
                NetNamedPipeBinding binding = new NetNamedPipeBinding();
                ServiceHost serviceHost = new ServiceHost(this, uri);
                serviceHost.AddServiceEndpoint(typeof(IWpkgNotifierMessageServer), binding, "");

                serviceHost.Open();

            }
            catch (Exception e)
            {
                eventLog.WriteEntry(e.Message, EventLogEntryType.Error);
                throw e;
            }

        }

        public void Register(int pid, Version clientVersion)
        {
            eventLog.WriteEntry(String.Format("Register requested for client pid {0} version {1}", pid, clientVersion.ToString()));
            IWpkgNotifierMessageClient client = OperationContext.Current.GetCallbackChannel<IWpkgNotifierMessageClient>();

            if(!(version.Major == clientVersion.Major && version.Minor == clientVersion.Minor))
            {
                client.Shutdown("A reboot is required. WpkgNotifier will now close.");
                eventLog.WriteEntry(String.Format("Shutting down client {0} due to version incompatibility", pid));
                // NB we still let the client be added briefly; it will automatically unregister itself when it
                // exits. 
            }

            clients.Add(pid, client);
        }

        public void Unregister(int pid)
        {
            if (clients.Remove(pid))
            {
                eventLog.WriteEntry(String.Format("Client {0} unregistered", pid));
            }
            else
            {
                eventLog.WriteEntry(String.Format("Unknown client {0} requested to unregister", pid), EventLogEntryType.Warning);
            }
        }
        public void ProcessRequest(WpkgMessageRequestType type)
        {

            switch (type)
            {
                case WpkgMessageRequestType.DO_SYNC:
                    RunWpkgSync();
                    break;
                case WpkgMessageRequestType.RUN_QUERY:
                    RunWpkgQuery();
                    break;
            }

        }

        private void SetClientsState(WpkgServiceState state)
        {
            foreach (KeyValuePair<int, IWpkgNotifierMessageClient> kv in clients)
            {
                try
                {
                    kv.Value.SetStatus(state);
                }
                catch (Exception e)
                {
                    eventLog.WriteEntry("Error communicating with client " + kv.Key + " " + e.ToString(), EventLogEntryType.Warning);
                }
            }
        }

        private void SendLogMessageToClients(string msg)
        {
            foreach (KeyValuePair<int, IWpkgNotifierMessageClient> kv in clients)
            {
                try
                {
                    string stampedMsg = String.Format("{0}: {1}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), msg);
                    kv.Value.LogMessage(stampedMsg);
                }
                catch (Exception e)
                {
                    eventLog.WriteEntry("Error communicating with client " + kv.Key + " " + e.ToString(), EventLogEntryType.Warning);
                }
            }
        }

        private void SendQueryResultToClients(WpkgQueryResult result)
        {
            foreach (KeyValuePair<int, IWpkgNotifierMessageClient> kv in clients)
            {
                try
                {
                    kv.Value.LogQueryResult(result);
                }
                catch (Exception e)
                {
                    eventLog.WriteEntry("Error communicating with client " + kv.Key + " " + e.ToString(), EventLogEntryType.Warning);
                }
            }
        }
    }
}
