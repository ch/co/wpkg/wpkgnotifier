﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.ServiceModel;
using System.Windows.Forms;

using WpkgUtilities;

namespace WpkgNotifier
{
    class TrayNotifier : ApplicationContext, IWpkgNotifierMessageClient
    {
        private NotifyIcon notifyIcon;
        private Version version;

        private EventLog eventLog;
        private IWpkgNotifierMessageServer server;
        private int pid;

        private StatusLines statusLines = new StatusLines();
        private StatusWindow statusWindow;
        private WpkgQueryResult LastQueryResult = new WpkgQueryResult();
        private WpkgStatusMessage statusMessage = new WpkgStatusMessage();

        public TrayNotifier()
        {
            Process currentProcess = Process.GetCurrentProcess();
            pid = currentProcess.Id;

            version = Assembly.GetExecutingAssembly().GetName().Version;

            eventLog = new EventLog();
            string eventSourceName = "TrayNotifier";
            string logName = "WpkgNotifier";

            //TODO: need to create the event source at a point where we have admin rights
            if (!EventLog.SourceExists(eventSourceName))
            {
                EventLog.CreateEventSource(eventSourceName, logName);
            }

            eventLog.Source = eventSourceName;
            eventLog.Log = logName;

            OpenMessageChannel();

            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);

            MenuItem showstatusMenuItem = new MenuItem("Status", new EventHandler(ShowStatusWindow));
            MenuItem showlogMenuItem = new MenuItem("View Log", new EventHandler(ShowLog));

            MenuItem manageSoftwarePage = new MenuItem("Manage software", new EventHandler(ManageSoftware));
            MenuItem exitMenuItem = new MenuItem("Exit", new EventHandler(Exit));

            notifyIcon = new NotifyIcon
            {
                Text = "WPKG notifier",
                Icon = WpkgNotifier.Properties.Resources.wpkgIcon,
                ContextMenu = new ContextMenu(new MenuItem[]
                {  showstatusMenuItem, manageSoftwarePage, showlogMenuItem, exitMenuItem }),
                Visible = true
            };

            // reflectively call our ShowContextMenu method upon left-click
            notifyIcon.MouseDown += (sender, e) =>
            {
                if (e.Button == MouseButtons.Left)
                {
                    MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                    if (mi != null)
                    {
                        mi.Invoke(notifyIcon, null);
                    }
                }
            };
        }

        public WpkgServiceState State { get; set; } = WpkgServiceState.IDLE;
        private void OnApplicationExit(object sender, EventArgs e)
        {
            server.Unregister(pid);
        }

        private void OpenMessageChannel()
        {
            try
            {
                NetNamedPipeBinding binding = new NetNamedPipeBinding();
                string uri = "net.pipe://localhost/www.ch.cam.ac.uk/WpkgNotifier";
                InstanceContext context = new InstanceContext(this);
                DuplexChannelFactory<IWpkgNotifierMessageServer> channelFactory =
                    new DuplexChannelFactory<IWpkgNotifierMessageServer>(context, binding, uri);
                
                server = channelFactory.CreateChannel();
                server.Register(pid, version);
                eventLog.WriteEntry("Registered with server");
            }
            catch (Exception e)
            {
                eventLog.WriteEntry(e.Message, EventLogEntryType.Error);
            }
        }

        void RequestSync(object sender, EventArgs e)
        {
            try
            {
                server.ProcessRequest(WpkgMessageRequestType.DO_SYNC);
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                ShowErrorAndExit();
            }
        }

        void RequestQuery(object sender, EventArgs e)
        {
            try
            {
                server.ProcessRequest(WpkgMessageRequestType.RUN_QUERY);
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                ShowErrorAndExit();
            }
        }

        void ShowErrorAndExit()
        {
            _ = MessageBox.Show("It was not possible to communicate with the WPKG Software Management service. This application must now exit.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Exit();
        }

        void ShowLog(object sender, EventArgs e)
        {
            StatusLogWindow statusLogWindow = new StatusLogWindow();
            statusLogWindow.BindStatusLines(statusLines);

            if (statusLogWindow.Visible)
            {
                statusLogWindow.Activate();
            }
            else
            {
                statusLogWindow.Show();
            }
        }

        void ShowStatusWindow(object sender, EventArgs e)
        {

            if (statusWindow == null || statusWindow.IsDisposed)
            {
                statusWindow = new StatusWindow();
                statusWindow.State = State;

                statusWindow.BindQueryResult(LastQueryResult);
                statusWindow.BindStatusMessage(statusMessage);
                statusWindow.SyncButtonClicked += RequestSync;
                statusWindow.QueryButtonClicked += RequestQuery;
            }

            if (statusWindow.Visible)
            {
                statusWindow.Activate();
            }
            else
            {
                statusWindow.Show();
            }
        }

        void ManageSoftware(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://apps.ch.cam.ac.uk/wpkg");
        }

        // Exit function with a signature compatible with being called
        // via an event handler
        void Exit(object sender, EventArgs e)
        {
            // We must manually tidy up and remove the icon before we exit.
            // Otherwise it will be left behind until the user mouses over.
            notifyIcon.Visible = false;
            Application.Exit();
        }

        void Exit()
        {
            // We must manually tidy up and remove the icon before we exit.
            // Otherwise it will be left behind until the user mouses over.
            notifyIcon.Visible = false;
            Application.Exit();
        }
        public void SetToolTip(string msg)
        {
            notifyIcon.Text = msg;
        }

        public void HandleResponse(WpkgMessageResponse response)
        {
            eventLog.WriteEntry(response.Message);
        }

        public void SetStatus(WpkgServiceState status)
        {
            this.State = status;
            SetStatusWindowState(State);

            switch (status)
            {
                case WpkgServiceState.DISCONNECTED:
                    notifyIcon.Icon = WpkgNotifier.Properties.Resources.grey;
                    statusMessage.SetMessage("Not currently connected");
                    SetToolTip("Not currently connected");
                    break;
                case WpkgServiceState.IDLE:
                    notifyIcon.Icon = WpkgNotifier.Properties.Resources.green;
                    statusMessage.SetMessage("No software changes are needed");
                    SetToolTip("No software changes are needed");
                    break;
                case WpkgServiceState.UPDATES_PENDING:
                    notifyIcon.Icon = WpkgNotifier.Properties.Resources.yellow;
                    statusMessage.SetMessage("Software changes are needed");
                    SetToolTip("Software changes are needed");
                    break;
                case WpkgServiceState.BUSY_QUERYING:
                    notifyIcon.Icon = WpkgNotifier.Properties.Resources.blue;
                    statusMessage.SetMessage("Checking for software changes");
                    SetToolTip("Checking for software changes...");
                    break;
                case WpkgServiceState.BUSY_SYNCING:
                    notifyIcon.Icon = WpkgNotifier.Properties.Resources.blue;
                    statusMessage.SetMessage("Applying software changes");
                    SetToolTip("Applying software changes...");
                    break;
            }
        }

        private void SetStatusWindowState(WpkgServiceState state)
        {
            if (statusWindow != null && !statusWindow.IsDisposed)
            {
                statusWindow.State = state;
            }
        }
        public void LogMessage(string msg)
        {
            statusLines.Add(msg);
            eventLog.WriteEntry(msg);
        }

        public void LogQueryResult(WpkgQueryResult result)
        {
            LastQueryResult.SetResults(result.PendingInstallations, result.PendingUpgrades, result.PendingRemovals);
            eventLog.WriteEntry(String.Format("Query results: {0}", LastQueryResult.StatusText));
        }

        public void Shutdown(string msg = null)
        {
            if (msg != null)
            {
                _ = MessageBox.Show(msg, "WpkgNotifier", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            Exit();
        }
    }
}
