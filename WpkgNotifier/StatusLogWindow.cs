﻿using System;
using System.Windows.Forms;

using WpkgUtilities;

namespace WpkgNotifier
{
    public partial class StatusLogWindow : Form
    {
        public StatusLogWindow()
        {
            InitializeComponent();
            this.Icon = WpkgNotifier.Properties.Resources.wpkgIcon;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(textBox.Text);
        }
        internal void BindStatusLines(StatusLines statusLines)
        {
            textBox.DataBindings.Add("Text", statusLines, "Lines", false, DataSourceUpdateMode.OnPropertyChanged, "Nothing found");
        }
    }
}
