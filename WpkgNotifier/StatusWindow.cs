﻿using System;
using System.Windows.Forms;
using WpkgUtilities;

namespace WpkgNotifier
{
    public partial class StatusWindow : Form
    {
        public event EventHandler SyncButtonClicked;
        public event EventHandler QueryButtonClicked;

        private WpkgServiceState state;
        public StatusWindow()
        {
            InitializeComponent();
        }

        public StatusWindow(WpkgServiceState state) : this()
        {
            State = state;
        }

        public WpkgServiceState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
                switch(state)
                {
                    case WpkgServiceState.DISCONNECTED:
                        Icon = Properties.Resources.grey;
                        break;
                    case WpkgServiceState.IDLE:
                        Icon = Properties.Resources.green;
                        SetQueryButtonEnabled(true);
                        SetSyncButtonEnabled(false);
                        break;
                    case WpkgServiceState.UPDATES_PENDING:
                        Icon = Properties.Resources.yellow;
                        SetQueryButtonEnabled(true);
                        SetSyncButtonEnabled(true);
                        break;
                    case WpkgServiceState.BUSY_QUERYING:
                        Icon = Properties.Resources.blue;
                        SetQueryButtonEnabled(false);
                        SetSyncButtonEnabled(false);
                        break;
                    case WpkgServiceState.BUSY_SYNCING:
                        Icon = Properties.Resources.blue;
                        SetQueryButtonEnabled(false);
                        SetSyncButtonEnabled(false);
                        break;
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SyncButton_Click(object sender, EventArgs e)
        {
            DialogResult = MessageBox.Show("Note: applying software changes may cause some programs to close, or make your computer reboot. Click OK to proceed, or Cancel if you want to apply the changes later.", "Apply software changes?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (DialogResult == DialogResult.OK)
            {
                SyncButtonClicked?.Invoke(this, e);
            }
        }

        private void queryButton_Click(object sender, EventArgs e)
        {
            QueryButtonClicked?.Invoke(this, e);
        }
        internal void BindQueryResult(WpkgQueryResult result)
        {
            pendingChangesTextBox.DataBindings.Add("Text", result, "StatusText", false, DataSourceUpdateMode.OnPropertyChanged, "Nothing found");
        }
        internal void BindStatusMessage(WpkgStatusMessage message)
        {
            statusTextBox.DataBindings.Add("Text", message, "StatusMessage", false, DataSourceUpdateMode.OnPropertyChanged, "");
        }

        private void SetQueryButtonEnabled(bool enabled)
        {
            queryButton.Enabled = enabled;
        }

        private void SetSyncButtonEnabled(bool enabled)
        {
            syncButton.Enabled = enabled;
        }

    }
}
